style_files := \
    beamercolorthemeYork169dark.sty \
    beamercolorthemeYork169light.sty \
    beamerthemeYork169dark.sty \
    beamerthemeYork169light.sty \
    beamerthemeYork169.sty \
    Yorkcolours.sty \

images := \
    images/YPI_main_page_dark.pdf \
    images/YPI_main_page_light.pdf \
    images/YPI_main_page_nologo_dark.pdf \
    images/YPI_main_page_nologo_light.pdf \
    images/YPI_title_page_dark.pdf \
    images/YPI_title_page_light.pdf \

tex_dir := ~/texmf/tex/latex/local/beamerthemeYork169

.PHONY: all

all: | $(tex_dir)
	cp -v $(style_files) $(tex_dir)
	cp -v $(images) $(tex_dir)/images

$(tex_dir):
	mkdir -p $@/images

uninstall:
	rm -v $(tex_dir)/*
	rmdir $(tex_dir)
